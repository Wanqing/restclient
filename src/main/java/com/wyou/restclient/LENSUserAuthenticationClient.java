package com.wyou.restclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.httpclient.util.URIUtil;

import com.google.gson.Gson;

class LENSAuthResponse {
	public boolean is_authorized;
	public String username;
	public String full_name;
	public String email;

	LENSAuthResponse() {

	}
}

public class LENSUserAuthenticationClient {

	public static void main(String[] args) {
		System.setProperty("https.protocols", "TLSv1.2");
		try {
			String username = "IDSAstra";
			String password = "IDSAstra#7";
			String lensurl = "https://lens.liaison.com";
			String apiURL = "/clientapi/1/profile/account/login.json?request.client_id=%s&request.client_secret=%s";
			String lensAuthenticationURL = lensurl + String.format(apiURL, username, password);

			URL requestURL = new URL(URIUtil.encodeQuery(lensAuthenticationURL));

			System.out.println("Request URL:" + requestURL);
			HttpURLConnection request = (HttpURLConnection) requestURL.openConnection();
			request.setRequestMethod("GET");
			request.setDoOutput(true);

			System.out.println("Starting to connect..");
			request.connect();
			System.out.println("Connected..Reading data..");
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			request.disconnect();
			System.out.println("Disconnecting... Data returned is: " + builder.toString());
			Gson gson = new Gson();
			LENSAuthResponse response = gson.fromJson(builder.toString(), LENSAuthResponse.class);
			System.out.println("Is user authorized: " + response.is_authorized);
			System.out.println("Authentication done.");
		}
		catch (IOException e) {
			System.out.println("Failed to load lens-ws.properties file");
		}
		catch (Exception e) {
			System.out.println("Error while authenticating against LENS");
		}

	}

}
