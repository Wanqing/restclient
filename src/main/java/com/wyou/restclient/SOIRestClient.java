package com.wyou.restclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import org.apache.commons.httpclient.util.URIUtil;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class SOIRestClient {

	public static void getMethod() {
		try {

			DefaultHttpClient httpClient = new DefaultHttpClient();
			String soiBaseURL = "http://at4p-lvdxpmts02.liaison.prod:34110/soi";
			String senderId = "xpx001";
			String receiverId = "mohawkp0000000000SOI";
			String serviceId = "service-router";
			String serviceDetail = "brunswick-ids-dealer-getpmfile";
			String queryString = "DealerID=000081341&Destination=PM";
			String requestUrl = String.format("%s/rest/%s/%s/%s/%s", soiBaseURL, senderId, receiverId, serviceId, serviceDetail);
			if (queryString != null && !queryString.isEmpty()) {
				requestUrl = requestUrl + "?" + queryString;
			}

			HttpGet getRequest = new HttpGet(URIUtil.encodePathQuery(requestUrl));
			System.out.println(" - sending request to: " + URIUtil.encodePathQuery(requestUrl));


			HttpResponse response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}

			httpClient.getConnectionManager().shutdown();

		}
		catch (ClientProtocolException e) {

			e.printStackTrace();

		}
		catch (IOException e) {

			e.printStackTrace();
		}

	}

	public static void postMethod() {
		try {

			DefaultHttpClient httpClient = new DefaultHttpClient();
			String soiBaseURL = "http://at4p-lvdxpmts01.liaison.prod:34010/soi";

			String senderId = "dealer";
			String receiverId = "BrunswickBoatGroup";
			String serviceId = "service-router";
			String serviceDetail = "brunswick-ids-dealer-getpmfile";
			String requestUrl = String.format("%s/rest/%s/%s/%s/%s", soiBaseURL, senderId, receiverId, serviceId, serviceDetail);

			HttpPost request = new HttpPost(URIUtil.encodePathQuery(requestUrl));

			StringEntity input = new StringEntity("DealerID=000081341&Destination=PM");
			request.setEntity(input);

			System.out.println(" - sending request to: " + URIUtil.encodePathQuery(requestUrl));

			HttpResponse response = httpClient.execute(request);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}

			httpClient.getConnectionManager().shutdown();

		}
		catch (MalformedURLException e) {

			e.printStackTrace();

		}
		catch (IOException e) {

			e.printStackTrace();

		}


	}

	public static void main(String[] args) {
		getMethod();
	}

}
