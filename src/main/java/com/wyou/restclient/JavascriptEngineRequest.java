package com.wyou.restclient;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

public class JavascriptEngineRequest {

	private final String scriptName;
	private final WorkflowIdentifier workflowIdentifier;
	private final String senderId;
	private final String receiverId;
	private final String documentContent;
	private final Reader contentReader;
	private final Map<String, String> dataItemSetMap;

	JavascriptEngineRequest(String scriptName, WorkflowIdentifier workflowIdentifier, String senderId, String receiverId, String documentContent,
			Reader contentReader, Map<String, String> dataItemSetMap) {
		super();
		this.scriptName = scriptName;
		this.workflowIdentifier = workflowIdentifier;
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.documentContent = documentContent;
		this.contentReader = contentReader;
		this.dataItemSetMap = (dataItemSetMap == null ? new HashMap<String, String>() : dataItemSetMap);
	}

	public final Reader getContentReader() {
		return contentReader;
	}

	/**
	 * @return the dataItemSet
	 */
	public final Map<String, String> getDataItemSet() {
		return dataItemSetMap;
	}

	/**
	 * @return the documentContent
	 */
	public final String getDocumentContent() {
		return documentContent;
	}

	/**
	 * @return the receiverId
	 */
	public final String getReceiverId() {
		return receiverId;
	}

	/**
	 * @return the scriptName
	 */
	public final String getScriptName() {
		return scriptName;
	}

	/**
	 * @return the senderId
	 */
	public final String getSenderId() {
		return senderId;
	}

	public final WorkflowIdentifier getWorkflowIdentifier() {
		return workflowIdentifier;
	}

	public final boolean isLargeContent() {
		return contentReader != null;
	}

	@Override
	public String toString() {
		return String.format("JavascriptEngineRequest[wid=%s,script=%s,sender=%s,receiver=%s", workflowIdentifier, scriptName, senderId, receiverId);
	}
}
