package com.wyou.restclient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.google.gson.Gson;

class JsonObject {

	private final String scriptLocation;
	private final String logFilePath;
	private final String scriptName;
	private final String requestId;
	private final String senderId;
	private final String receiverId;
	private final String documentContent;
	private final String workflowIdentifier;
	private final Map<String, String> dataItemSetMap;


	JsonObject(JavascriptEngineRequest javascriptEngineRequest, String documentContent, String scriptLocation, String logFilePath) {
		this(
				scriptLocation,
				logFilePath,
				javascriptEngineRequest.getScriptName(),
				javascriptEngineRequest.getWorkflowIdentifier().getRequestId(),
				javascriptEngineRequest.getSenderId(),
				javascriptEngineRequest.getReceiverId(),
				documentContent,
				javascriptEngineRequest.getDataItemSet(),
				javascriptEngineRequest.getWorkflowIdentifier().getStringRepresentation());
	}

	private JsonObject(String scriptLocation, String logFilePath, String scriptName, String requestId, String senderId, String receiverId,
			String documentContent, Map<String, String> dataItemSetMap, String workflowIdentifier) {
		this.scriptLocation = scriptLocation;
		this.logFilePath = logFilePath;
		this.scriptName = scriptName;
		this.requestId = requestId;
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.documentContent = documentContent;
		this.dataItemSetMap = (dataItemSetMap);
		this.workflowIdentifier = workflowIdentifier;
	}

	public final Map<String, String> getDataItemSet() {
		return dataItemSetMap;
	}

	public final String getDocumentContent() {
		return documentContent;
	}

	public final String getLogFilePath() {
		return logFilePath;
	}

	public final String getReceiverId() {
		return receiverId;
	}

	public final String getRequestId() {
		return requestId;
	}

	public final String getScriptLocation() {
		return scriptLocation;
	}

	public final String getScriptName() {
		return scriptName;
	}

	public final String getSenderId() {
		return senderId;
	}

	public String getWorkflowIdentifier() {
		return workflowIdentifier;
	}
}

class JavascriptEngineResponse {

	private final String documentContent;
	private final Map<String, String> dataItemSet;

	public JavascriptEngineResponse(String documentContent, Map<String, String> dataItemSet) {
		this.documentContent = documentContent;
		this.dataItemSet = (dataItemSet);
	}

	public int size() {
		return dataItemSet.size();
	}

	public String get(String key) {
		return dataItemSet.get(key);
	}

	public Set<String> keySet() {
		return dataItemSet.keySet();
	}

	/**
	 * @return the documentContent
	 */
	public String getDocumentContent() {
		return documentContent;
	}
}

class WorkflowIdentifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5945201427875273360L;

	/**
	 * Creates a new instance without parent with the specified request id and step number.
	 * @param requestId
	 * @param stepNo
	 * @return
	 * @throws IllegalArgumentException if requestId is null or empty; or stepNo is null or less than one
	 */
	public static WorkflowIdentifier createInstance(final String requestId, final Integer stepNo) {
		if (stepNo == null) {
			throw new IllegalArgumentException("stepNo cannot be null");
		}
		return new WorkflowIdentifier(requestId, null, stepNo);
	}

	/**
	 * Creates a new  instance with the specified request id, parent, and setp number.
	 * @param parent
	 * @param stepNo
	 * @return
	 * @throws IllegalArgumentException if requestId is null or empty, or parent is null, or stepNo is null or less than one
	 */
	public static WorkflowIdentifier createInstance(final WorkflowIdentifier parent, final Integer stepNo) {
		if (parent == null) {
			throw new IllegalArgumentException("parent cannot be null");
		}
		if (stepNo == null) {
			throw new IllegalArgumentException("stepNo cannot be null");
		}
		return new WorkflowIdentifier(parent.getRequestId(), parent, stepNo);
	}

	/**
	 * Creates a new root instance with the specified request id.
	 * @param requestId
	 * @return
	 * @throws IllegalArgumentException if requestId is null or empty
	 */
	public static WorkflowIdentifier createRootInstance(final String requestId) {
		return new WorkflowIdentifier(requestId, null, null);
	}

	/**
	 * Creates new instance from the specified string representation.
	 * Use {@link #getStringRepresentation()} to obtain a string representation.
	 * 
	 * @param stringRepresentation
	 * @return new instance from <code>stringRepresenetation</code>
	 * @see #getStringRepresentation()
	 * @throws IllegalArgumentException if stringRepresentation is null or empty
	 * @throws NumberFormatException if stringRepresentation contains invalid tokens
	 */
	public static WorkflowIdentifier valueOf(final String stringRepresentation) {
		if (stringRepresentation == null || stringRepresentation.isEmpty()) {
			throw new IllegalArgumentException("stringRepresentation cannot be null or empty");
		}

		String[] tokens = stringRepresentation.split("\\:");
		WorkflowIdentifier workflowIdentifier;
		String requestId = tokens[0];
		if (tokens.length == 1) {
			workflowIdentifier = createRootInstance(requestId);
		}
		else {
			workflowIdentifier = createInstance(requestId, Integer.valueOf(tokens[1]));
			if (tokens.length > 2) {
				for (int i = 2; i < tokens.length; i++) {
					workflowIdentifier = createInstance(workflowIdentifier, Integer.valueOf(tokens[i]));
				}
			}
		}
		return workflowIdentifier;
	}

	private final String requestId;
	private final WorkflowIdentifier parent;
	private final Integer stepNo;
	private final String stringRepresentation;

	private WorkflowIdentifier(final String requestId, final WorkflowIdentifier parent, final Integer stepNo) {
		this.requestId = requestId;
		this.parent = parent;
		this.stepNo = stepNo;

		checkInvariants();

		StringBuilder stringBuilder = new StringBuilder();
		if (parent != null) {
			stringBuilder.append(parent.stringRepresentation);
		}
		else {
			stringBuilder.append(requestId);
		}
		if (stepNo != null) {
			stringBuilder.append(":");
			stringBuilder.append(stepNo);
		}
		this.stringRepresentation = stringBuilder.toString();
	}

	private void checkInvariants() {
		if (requestId == null || requestId.isEmpty()) {
			throw new IllegalArgumentException("requestId cannot be null or empty");
		}
		if (stepNo != null && stepNo < 1) {
			throw new IllegalArgumentException("stepNo must be positive");
		}
		if (parent != null && stepNo == null) {
			throw new IllegalArgumentException("stepNo is required with parent");
		}
	}

	public WorkflowIdentifier getParent() {
		return parent;
	}

	public String getRequestId() {
		return requestId;
	}

	public Integer getStepNo() {
		return stepNo;
	}

	/**
	 * Gets the string representation of this.
	 * Use {@link #valueOf(String)} to create a new instance of this from the returned value.
	 * @return string representation
	 * @see #valueOf(String)
	 */
	public String getStringRepresentation() {
		return stringRepresentation;
	}

	public boolean isRoot() {
		return parent == null && stepNo == null;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (WorkflowIdentifier.class.isInstance(obj)) {
			return stringRepresentation.equals((WorkflowIdentifier.class.cast(obj).stringRepresentation));
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return stringRepresentation.hashCode();
	}

	@Override
	public String toString() {
		return String.format("WorkflowIdentifier[%s]", stringRepresentation);
	}

	// Serialization Support ////////////////////////////////////////////////////////////

	private void writeObject(ObjectOutputStream s) throws IOException {
		s.defaultWriteObject();
	}

	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		s.defaultReadObject();
		try {
			checkInvariants();
		}
		catch (IllegalArgumentException e) {
			throw new InvalidObjectException(e.getMessage());
		}
	}

}


public class TestJSInvocation {

	public static void main(String[] args) throws Exception {
		TestJSInvocation test = new TestJSInvocation();
		String senderId = "dealer";
		String receiverId = "BrunswickBoatGroup";
		String serviceId = "service-router";
		String serviceDetail = "brunswick-ids-dealer-getpmfile";
		String queryString = "DealerID=000081341&Destination=PM";
		Map<String, String> map = new HashMap<String, String>();
		//		map.put("DealerID", "000081341");
		//		map.put("Destination", "PM");
		WorkflowIdentifier identifier = WorkflowIdentifier.createRootInstance(serviceId + ":" + serviceDetail);
		JavascriptEngineRequest request = new JavascriptEngineRequest(
				"brunswick-ids-dealer-getpmfile.js",
				identifier,
				senderId,
				receiverId,
				queryString,
				null,
				map);
		System.out.println(request.getDocumentContent());
		test.invokeScript(request);
	}

	public void invokeScript(JavascriptEngineRequest request) throws Exception {
		String DEFAULT_FUNCTION_NAME = "javascriptEngine_main_actual";
		if (request == null) {
			throw new IllegalArgumentException("request cannot be null");
		}
		if (request.getScriptName() == null || request.getScriptName() == "") {
			throw new IllegalArgumentException("request.scriptName cannot be null or empty");
		}

		String scriptFilename = "brunswick-ids-dealer-getpmfile.js";

		System.out.println(String.format("---> invokeScript: [%s] for [%s]", scriptFilename, request.getWorkflowIdentifier().getStringRepresentation()));

		// TODO Performance: We probably want to cache this for performance gain (SOI-427)
		String scriptLocation = "src/test/data/scripts";
		String logFilePath = "src/test/data/log.log";
		long startTime = System.currentTimeMillis();

		// load script from disk
		System.out.println(String.format("loading content of [%s]", scriptFilename));

		StringBuilder scriptContent = new StringBuilder(loadScript(scriptLocation, scriptFilename));

		// TODO Performance: Do we must read it every time? Can we cache it at startup? (SOI-427)
		scriptContent.append("\n");
		scriptContent.append(loadScript("src/main/resources", "javascript-engine-api.js"));

		// TODO Performance: Do we have to initialize engine everytime? Can we cache it at startup? (SOI-427)
		// initialize javascript
		ScriptEngineManager engineMgr = new ScriptEngineManager();
		ScriptEngine engine = engineMgr.getEngineByName("JavaScript");

		String stringResponse = null;
		try {
			// create object for JSON-serialization
			JsonObject jsonObject = new JsonObject(request, getDocumentContent(request), scriptLocation, logFilePath);

			Gson gson = new Gson();
			String stringRequest = gson.toJson(jsonObject);

			System.out.println(String.format("JSON input to javascript:%n%s", stringRequest));

			// invoke script
			engine.eval(scriptContent.toString());
			Invocable invocableEngine = (Invocable) engine;
			stringResponse = (String) invocableEngine.invokeFunction(DEFAULT_FUNCTION_NAME, stringRequest);

			System.out.println(String.format("JSON output from javascript:%n%s", stringResponse));

			System.out.println(
					String.format(
							"Invoked script [%s] from [%s]; elapsed time=%d (ms)",
							scriptFilename,
							request.getScriptName(),
							(System.currentTimeMillis() - startTime)));

			// convert string response from script to JavascriptEngineResponse object
			JavascriptEngineResponse response = gson.fromJson(stringResponse, JavascriptEngineResponse.class);

			System.out.println(String.format("<--- invokeScript: [%s]", scriptFilename, response));

			//			return response;
		}
		catch (NoSuchMethodException e) {
			throw new Exception(
					String.format(
							"Entry function to the script needs to be named 'main' (script=%s,wid=%s)",
							request.getScriptName(),
							request.getWorkflowIdentifier().getStringRepresentation()));
		}
		catch (ScriptException e) {
			throw new Exception(
					String.format(
							"Error occured while running script: %s (wid=%s)",
							request.getScriptName(),
							request.getWorkflowIdentifier().getStringRepresentation()),
					e);
		}
		catch (IOException e) {
			throw new Exception(String.format("I/O Error occured while processing large content for %s", request.getWorkflowIdentifier()), e);
		}
	}

	//---- private functions ----//
	private String loadScript(String scriptLocation, String scriptFilename) throws Exception {
		StringBuilder stringBuilder = new StringBuilder();
		Reader reader = null;
		try {
			reader = new BufferedReader(new FileReader(new File(scriptLocation, scriptFilename)));

			char[] buffer = new char[1024];
			int readCount = 0;

			while ((readCount = reader.read(buffer)) != -1) {
				stringBuilder.append(buffer, 0, readCount);
			}

			return stringBuilder.toString();
		}
		catch (FileNotFoundException e1) {
			throw new Exception(String.format("Javascript file named \"%s\" was not found in %s.", scriptFilename, scriptLocation));
		}
		catch (IOException e) {
			throw new Exception(String.format("IOException while reading script content from file: %s in %s", scriptFilename, scriptLocation), e);
		}
		finally {
			if (reader != null) reader.close();
		}
	}

	private static String getDocumentContent(JavascriptEngineRequest javascriptEngineRequest) throws IOException {
		return javascriptEngineRequest.getDocumentContent();
	}

	private String loadEngineAPI() throws Exception {
		try {
			return readAllAsString(new InputStreamReader(getClass().getResourceAsStream("javascript-engine-api.js")));
		}
		catch (IOException e) {
			throw new IOException("IOException while reading engine API from classpath");
		}
		catch (Exception e) {
			throw new Exception("Unhandled exception while reading engine API from classpath");
		}
	}

	public static String readAllAsString(Reader reader) throws IOException {
		return readAllAsString(new BufferedReader(reader));
	}

	public static String readAllAsString(BufferedReader reader) throws IOException {
		try {
			StringWriter sw = new StringWriter();
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				sw.append(line);
				sw.append(System.getProperty("line.separator"));
			}
			sw.flush();
			return sw.toString();
		}
		finally {
			if (reader != null) reader.close();
		}
	}

}
