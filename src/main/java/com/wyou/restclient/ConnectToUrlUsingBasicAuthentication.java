package com.wyou.restclient;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

public class ConnectToUrlUsingBasicAuthentication {
	private List<String> lensInternalIPs;

	public static void main(String[] args) {
		System.setProperty("https.protocols", "TLSv1,TLSv1.2");
		sendRequest();
	}

	public static void sendRequest() {
		try {
			//			String webPage = "https://lens-ws.liaison.com/soi/rest/dealer/BrunswickBoatGroup/service-router/brunswick-ids-dealer-getpmfile";
			String webPage = "https://at4u-lvdxpmtw01.liaison.prod:34543/visibility/soi/rest/dealer/BrunswickBoatGroup/service-router/brunswick-ids-dealer-getpmfile";

			String name = "IDSAstra";
			String password = "IDSAstra#7";

			String authString = name + ":" + password;
			System.out.println("auth string: " + authString);
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);
			System.out.println("Base64 encoded auth string: " + authStringEnc);

			URL url = new URL(webPage);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", String.format("Basic %s", authStringEnc));
			HttpURLConnection htcon = (HttpURLConnection) urlConnection;

			String input_format = "DealerID=0000000320&Destination=SR";

			byte[] postData = input_format.getBytes(StandardCharsets.UTF_8);
			int postDataLength = postData.length;
			htcon.setDoOutput(true);
			htcon.setInstanceFollowRedirects(false);
			htcon.setRequestMethod("POST");
			htcon.setRequestProperty("Content-Type", "application/xml");
			htcon.setRequestProperty("charset", "utf-8");
			htcon.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			htcon.setUseCaches(false);
			try (DataOutputStream wr = new DataOutputStream(htcon.getOutputStream())) {
				wr.write(postData);
			}

			InputStream is = htcon.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);

			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			String result = sb.toString();

			System.out.println("*** BEGIN ***");
			//			System.out.println(result);//
			System.out.println("*** END ***");
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
