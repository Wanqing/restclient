package com.wyou.restclient;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class VANTPClient {

	public static void main(String[] args) {
		String input_format = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><tradingpartners><tradingpartner qualifier=\"ZZ\" traderId=\"cswar1\"/></tradingpartners>";

		try {
			retrievePacketList(input_format);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void retrievePacketList(String xmlString) throws Exception {
		DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = null;

		document = docBuilder.parse(IOUtils.toInputStream(xmlString));
		document.getDocumentElement().normalize();

		NodeList nodeList = document.getElementsByTagName("tradingpartner");

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);

			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				String tradingId = element.getAttribute("qualifier") + element.getAttribute("traderId");
				System.out.println(String.format("tradingId:%s", tradingId));
				System.out.println(String.format("right pad:%s", StringUtils.rightPad(tradingId, 39)));
				System.out.println(String.format("network:%s", element.getAttribute("network")));
				System.out.println(String.format("tradingName:%s", element.getAttribute("name")));
				System.out.println(String.format("tradingQual:%s", element.getAttribute("qualifier")));
			}
		}
	}


	public static void postMethod() {
		try {
			String webPage = "http://at4p-lvdxpmtw02.liaison.prod:34110/visibility/van/tradingpartner/search.xml";
			String name = "wyou";
			String password = "FgM6wU2C";

			String authString = name + ":" + password;
			System.out.println("auth string: " + authString);
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);
			System.out.println("Base64 encoded auth string: " + authStringEnc);

			URL url = new URL(webPage);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", String.format("Basic %s", authStringEnc));
			HttpURLConnection htcon = (HttpURLConnection) urlConnection;

			String input_format = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><tradingpartners><tradingpartner qualifier=\"ZZ\" traderId=\"cswar1\"/></tradingpartners>";

			byte[] postData = input_format.getBytes(StandardCharsets.UTF_8);
			int postDataLength = postData.length;
			htcon.setDoOutput(true);
			htcon.setInstanceFollowRedirects(false);
			htcon.setRequestMethod("POST");
			htcon.setRequestProperty("Content-Type", "application/xml");
			htcon.setRequestProperty("charset", "utf-8");
			htcon.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			htcon.setUseCaches(false);
			try (DataOutputStream wr = new DataOutputStream(htcon.getOutputStream())) {
				wr.write(postData);
			}

			InputStream is = htcon.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);

			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			String result = sb.toString();

			System.out.println("*** BEGIN ***");
			System.out.println(result);
			System.out.println("*** END ***");
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
